\renewcommand{\FIGREP}{src/git/figures}

\section{Handling source code with Git}
\label{sec:git}
\intersec{fidis}

\subsection{Resources}
\begin{frame}[fragile]
	\frametitle{Resources}
	\framesubtitle{}

	\begin{itemize}
		\item Git official documentation:\newline
		      \url{https://git-scm.com/doc}
		\item Pro Git, Scott Chacon and Ben Straub:\newline
		      \url{https://git-scm.com/book/en/v2}
		\item Many images are taken from this book.
		\item A funny game to learn Git interactively:\newline
		      \url{https://learngitbranching.js.org/}
	\end{itemize}
\end{frame}

\subsection{What is version control?}
\begin{frame}[fragile, t]
	\frametitle{What is version control and why should you care?}
	\framesubtitle{}

	\begin{itemize}
		\item Version control is the act of recording the state of files over time.
		      \pause
		      \begin{itemize}
			      \item You have certainly all used this archaic version control
		      \end{itemize}
		      \vspace{1.2cm}
		      \pause
		\item Version control system (VCS) is a software that automates version
		      control.
		\item In particular, it allows you to:
		      \begin{itemize}
			      \item revert to a specific state,
			      \item compare two states,
			      \item see who last modified a file,
			      \item etc.
		      \end{itemize}
		      \pause
		\item These capabilities are very handy because:
		      \begin{itemize}
			      \item Bugs will be introduced,
			      \item You want to test different developments,
			      \item Sometimes you need a timestamp on some code, \eg{} for patent issues.
		      \end{itemize}
	\end{itemize}
	\onslide<2->\addimage[width=10cm]{\FIGREP/vcs_files}{4cm}{5.7cm}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Different types of VCSs}
	\framesubtitle{}

	\begin{minipage}{0.3\linewidth}
		\textbf{Local VCS}
		\begin{itemize}
			\item A unique repository is created on your computer.
			\item It contains the files as well as a ``version database''.
			\item Contribution from one user.
			\item For example, RCS.
		\end{itemize}
	\end{minipage}
	\hfill
	\pause
	\begin{minipage}{0.3\linewidth}
		\textbf{Centralized VCS}
		\begin{itemize}
			\item There is a single shared repository.
			\item It's the single source of truth.
			\item Everyone pulls/pushes information from/to it.
			\item For example, CVS and Subversion.
		\end{itemize}
	\end{minipage}
	\hfill
	\pause
	\begin{minipage}{0.3\linewidth}
		\textbf{Distributed VCS}
		\begin{itemize}
			\item Everyone gets the whole repo.
			\item No single source of truth anymore.
			\item Can pull from one and push to the other.
			\item For example Git and Mercurial.
		\end{itemize}
	\end{minipage}
	\onslide<1->\addimage[width=3cm]{\FIGREP/local}{2.5cm}{1.0cm}
	\onslide<2->\addimage[width=4cm]{\FIGREP/centralized}{6.5cm}{2.0cm}
	\onslide<3->\addimage[width=3cm]{\FIGREP/distributed}{11.5cm}{0.5cm}
\end{frame}

\subsection{A brief history of Git}
\begin{frame}[fragile]
	\frametitle{A brief history of Git}
	\framesubtitle{}

	\begin{itemize}
		\item Git development started in 2005 by Linus Torvalds.
		\item The goal was to replace BitKeeper for the Linux kernel development.
		\item The design goals were:
		      \begin{itemize}
			      \item Speed
			      \item Simple design
			      \item Strong support for non-linear development
			      \item Fully distributed
			      \item Able to handle large projects efficiently
		      \end{itemize}
	\end{itemize}
	\addimage[width=2cm]{\FIGREP/Linus_Torvalds}{13cm}{3.0cm}
	\onslide<2>\addimage[width=3cm]{\FIGREP/Git}{6.5cm}{1.0cm}
\end{frame}

\subsection{Git internals}
\begin{frame}[fragile, t]
	\frametitle{Git internals}
	\framesubtitle{Snapshots!}
	\begin{itemize}
		\item Most of the VCSs store information as a list of file-based changes:
	\end{itemize}
	\pause
	\vspace{3cm}
	\begin{itemize}
		\item Git, on the other hand, stores them as whole-file snapshots:
	\end{itemize}

	\onslide<1->\addimage[width=6cm]{\FIGREP/deltas}{5cm}{4.2cm}
	\onslide<2->\addimage[width=6cm]{\FIGREP/snapshots}{5cm}{0.5cm}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Git internals}
	\framesubtitle{Local operations and integrity}

	\begin{itemize}
		\item Almost every operation in Git is local
		      \begin{itemize}
			      \item No latency due to network
			      \item Can work even without an internet connection
		      \end{itemize}
		      \pause
		\item Git has integrity
		      \begin{itemize}
			      \item Everything in Git is checksummed before being stored
			      \item Impossible to change the content of any file without Git
			            knowing it
			      \item Git uses SHA-1 hashes (40-character string):\newline
			            \code{24b9da6552252987aa493b52f8696cd6d3b00373}
		      \end{itemize}
		      \pause
		\item Git usually only adds data to the database. It is difficult to lose
		      information once committed.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Git internals}
	\framesubtitle{The holy four states!}
	\begin{itemize}
		\item Git has four main states in which a file can reside in,
		      \textit{untracked}, \textit{modified},
		      \textit{staged}, \textit{committed}:
		      \begin{itemize}
			      \item Untracked means that the file is not tracked by Git.
			      \item Modified means that you have changed the file, but not saved
			            it to the database yet.
			      \item Staged means that the file is marked to be included in the
			            next commit.
			      \item Committed means that the data is safely stored into the local database.
		      \end{itemize}
	\end{itemize}

	\onslide<1->\addimage[width=6cm]{\FIGREP/areas}{5cm}{0.5cm}
\end{frame}

\subsection{Using Git locally to save you work}
\subsubsection{Getting a repository}
\begin{frame}[fragile]
	\frametitle{Getting a repository}
	\framesubtitle{}

	\hfill
	\begin{minipage}{0.8\linewidth}
		With Git, there are two ways of getting a repository:
		\begin{itemize}
			\item Initialize an empty repository:
			      \begin{consoleoutput}
				      $ git init
				      Initialized empty Git repository in /home/user/my_code/.git/
			      \end{consoleoutput}
			\item Get an existing repository:
			      \begin{consoleoutput}
				      $ git clone <repository address> Cloning into 'my_code'... remote:
				      Counting objects: 30989, done. remote: Compressing objects:
				      100% (7773/7773), done.
				      remote: Total 30989 (delta 23002), reused 30984 (delta 22999)
				      Receiving objects:
				      100% (30989/30989), 74.67 MiB | 14.82 MiB/s, done.
				      Resolving deltas: 100% (23002/23002), done.
			      \end{consoleoutput}
		\end{itemize}
	\end{minipage}
	\onslide<1>\addimage[width=3cm]{\FIGREP/git_clone_0}{1cm}{1.5cm}
	\onslide<2>\addimage[width=3cm]{\FIGREP/git_clone_1}{1cm}{1.5cm}
	\onslide<3>\addimage[width=3cm]{\FIGREP/git_clone_99}{1cm}{1.5cm}
\end{frame}

\subsubsection{Getting a repository}
\begin{frame}[fragile, exercise]
	\frametitle{Getting a repository}
	\framesubtitle{}

	\begin{itemize}
		\item Make sure you have Git installed by typing
		      \begin{consoleoutput}
			      $ git --version
			      git version 2.39.2
		      \end{consoleoutput}
		\item Clone the following repo: \newline
		      \code{https://gitlab.epfl.ch/lanti/data-management-exercises.git}
		\item Go into the exercise folder and initilize it:\newline
		      \code{cd data-management-exercises}\newline
		      \code{./setup.sh}
	\end{itemize}
\end{frame}

\subsubsection{Checking the status}
\begin{frame}[fragile]
	\frametitle{Checking the status}
	\framesubtitle{\code{git status}}

	To check the status of your repository, use \code{git status}.
	\pause
	\begin{consoleoutput}
		$ git status
		On branch main
		Changes to be committed:
		(use "git restore --staged <file>..." to unstage)
		modified:   file_1.txt

		Changes not staged for commit:
		(use "git add/rm <file>..." to update what will be committed)
		(use "git restore <file>..." to discard changes in working directory)
		modified:   file_1.txt
		modified:   file_2.txt
		deleted:    file_4.txt

		Untracked files:
		(use "git add <file>..." to include in what will be committed)
		file_5.txt
	\end{consoleoutput}

\end{frame}

\subsubsection{Tracking your changes}
\begin{frame}[fragile]
	\frametitle{Tracking your changes}
	\framesubtitle{\code{git add}}

	To add a file to the staging area, use \code{git add <file name>}
	\onslide<1->\begin{consoleoutput}
		$ git status
		On branch main
		Changes not staged for commit:
		(use "git add <file>..." to update what will be committed)
		(use "git restore <file>..." to discard changes in working directory)
		modified:   file_1.txt

		no changes added to commit (use "git add" and/or "git commit -a")
	\end{consoleoutput}

	\onslide<2->\begin{consoleoutput}
		$ git add file_1.txt
	\end{consoleoutput}

	\onslide<3>\begin{consoleoutput}
		$ git status
		On branch main
		Changes to be committed:
		(use "git restore --staged <file>..." to unstage)
		modified:   file_1.txt
	\end{consoleoutput}
\end{frame}

\subsubsection{Committing your changes}
\begin{frame}[fragile, t]
	\frametitle{Committing your changes}
	\framesubtitle{\code{git commit}}

	Now that we have staged some files, we can save our work using \code{git
		commit}
	\begin{consoleoutput}
		$ git commit -m <commit message>
			[main 52aafd1] <commit message>
		1 file changed, 1 insertion(+)
	\end{consoleoutput}
	If you omit the \code{-m} option, it will open the default editor to input your message.

	\onslide<2>\addimage[width=3cm]{\FIGREP/git_staging_0}{6.5cm}{0.5cm}
	\onslide<3>\addimage[width=3cm]{\FIGREP/git_staging_1}{6.5cm}{0.5cm}
	\onslide<4>\addimage[width=3cm]{\FIGREP/git_staging_2}{6.5cm}{0.5cm}
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{Committing your changes}
	\framesubtitle{\code{git commit}}

	\textbf{A few tips}
	\begin{itemize}
		\item Commit related changes
		\item Commit often
		\item Don't commit half-done work
		\item Write good commit messages
		      \begin{itemize}
			      \item Capitalized, short (50 chars or less) summary
			      \item Always leave the second line blank
			      \item More detailed explanatory text, if necessary
			      \item Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
		      \end{itemize}
	\end{itemize}
	\addimage[width=4cm]{\FIGREP/git_commit}{6cm}{1.0cm}
\end{frame}

\subsubsection{Ignoring files}
\begin{frame}[fragile]
	\frametitle{Ignoring files}
	\framesubtitle{}

	\begin{itemize}
		\item There are files you will never want to include in the repository,
		      \eg{} \code{*.o}, CMake \code{build} directory, etc.
		\item To avoid mistakes, Git has a special file, \code{.gitignore}, to list excluded files
	\end{itemize}
	\pause
	\begin{consoleoutput}
		# ignore all .a files
		*.a
		# but do track lib.a, even though you're ignoring .a files above
		!lib.a
		# only ignore the TODO file in the current directory, not subdir/TODO
		/TODO
		# ignore all files in any directory named build
		build/
		# ignore doc/notes.txt, but not doc/server/arch.txt
		doc/*.txt
		# ignore all .pdf files in the doc/ directory and any of its subdirectories
		doc/**/*.pdf
	\end{consoleoutput}
\end{frame}

\subsubsection{Viewing the history}
\begin{frame}[fragile]
	\frametitle{Viewing the history}
	\framesubtitle{\code{git log}}

	\begin{itemize}
		\item One of the strength of Git is to be able to quickly inspect the whole
		      history of your project.
		\item This is achieved using the \code{git log} command
		      \begin{consoleoutput}
			      commit 0d96e54298920a5d05abfecf62e002ad0b4281c2 (HEAD -> new_slides_2024)
			      Author: John Doe <john.doe@epfl.ch>
			      Date:   Mon May 13 10:43:43 2024 +0200

			      Add skeleton for new course

			      commit 53edfad5d2cc8ad1ffe9e9073ac71b4a88231f3e
			      Author: John Doe <john.doe@epfl.ch>
			      Date:   Mon May 13 09:06:40 2024 +0200

			      Remove previous content

			      commit bedb8f7c493ca82e365b8b7266454b93aa956422 (origin/main, origin/HEAD, main)
			      Author: Jane Doe <jane.doe@epfl.ch>
			      Date:   Wed Feb 16 11:46:56 2022 +0100

			      Minor updates for 2022 February course
		      \end{consoleoutput}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Viewing the history}
	\framesubtitle{\code{git log}}

	\begin{itemize}
		\item There are many options to \code{git log} that allow you to get various
		      information.
		\item For example:
		      \begin{description}
			      \item[\code{-{}-patch}]: Show the changes made in the commits,
			      \item[\code{-{}-oneline}]: Show a shortened version of the log,
			      \item[\code{-{}-graph}]: Print the log in a graph form,
			      \item[\code{-{}-format}]: Change the way (the format) the log is printed.
		      \end{description}
	\end{itemize}
	\begin{consoleoutput}
		| *   29e379b - John Doe - Merge branch 'specific/bern'
		| |\
		| | * b4355e6 - Jane Doe - Use a shellscript(7 years ago)
		| | * 359aa4c - Jane Doe - Build the slides with docker(7 years ago)
		| * | e86e31e - John Doe - Merge branch 'specific/bern'
		| * | 5b2690c - John Doe - adding remote/conflicts(7 years ago)
		| |/
		| * 6498225 - John Doe - adding add/commit/push(7 years ago)
		| * 52098e9 - Jane Doe - persons->people(7 years ago)
		|/
		* c4b2504 - John Doe - Update scratch(7 years ago)
	\end{consoleoutput}
\end{frame}

\begin{frame}[fragile, exercise]
	\frametitle{Basic commands}
	\framesubtitle{}

	\begin{itemize}
		\item Use \code{git status} to see on which branch you are on. How is your
		      working directory? Would you expect anything different?
		\item Create a file.
		\item What does \code{git status} output look like now?
		\item Add the file to the staging area. How does \code{git status} look now?
		\item Commit the file to the repository. How does \code{git status} look
		      now?
		\item Inspect the history of the repository using \code{git log}:
		      \begin{itemize}
			      \item Who made the first commit, and when?
			      \item What is the hash of the commit adding the \code{README.md}
			            file?
			      \item Try different options, \eg{} \code{-{}-oneline},
			            \code{-{}-graph}, and \code{-{}-patch}.
		      \end{itemize}
		\item Change an existing file. How does \code{git status} look now?
		\item Commit the changes.
		\item Execute the following command:\newline
		      \code{./utils/generate\_files.sh}
		\item Configure Git to accept only PDF files that are in \code{images/cats}
		      and subfolders.
		\item Commit those images.
	\end{itemize}
\end{frame}

\subsubsection{Viewing the differences}
\begin{frame}[fragile]
	\frametitle{Viewing the differences}
	\framesubtitle{\code{git diff}}

	\begin{itemize}
		\item One of the very important feature of Git is to easily get the
		      differences between two files.
		\item This is done using \code{git diff}.
	\end{itemize}
	\begin{consoleoutput}
		$ git status
		On branch main
		Changes not staged for commit:
		(use "git add <file>..." to update what will be committed)
		(use "git restore <file>..." to discard changes in working directory)
		modified:   program.cpp

		no changes added to commit (use "git add" and/or "git commit -a")
	\end{consoleoutput}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Viewing the differences}
	\framesubtitle{\code{git diff}}

	\begin{itemize}
		\item One of the very important feature of Git is to easily get the
		      differences between two files.
		\item This is done using \code{git diff}.
	\end{itemize}

	\begin{consoleoutput}
		$ git diff
		diff --git a/program.cpp b/program.cpp
		index f37606f..76edb6c 100644
		--- a/program.cpp
		+++ b/program.cpp
		@@ -2,7 +2,7 @@

		int main(int argc, char* argv[]) {

		-  if (argc = 1) {
		+  if (argc == 1) {
				std::cout << "Not enough arguments\n";
			}
	\end{consoleoutput}
	\pause
	\begin{itemize}
		\item If files are already staged, you can use \code{git diff -{}-staged}
	\end{itemize}
\end{frame}

% \subsubsection{Undoing things}
% \begin{frame}[fragile]
% 	\frametitle{}
% 	\framesubtitle{}

% \end{frame}

\subsubsection{Tagging}
\begin{frame}[fragile]
	\frametitle{Tagging the history}
	\framesubtitle{\code{git tag}}
	\begin{itemize}
		\item Certain commits are of particular importance, \eg{} bug fixes, new
		      release, results for paper, \textit{etc}.
		\item Git allows you to tag them to be able to refer to them easily.
		\item All the tag related actions are done using the \code{git tag} command. Use:
		      \begin{itemize}
			      \item \code{git tag} to list all the tags,
			      \item \code{git tag <name>} to create a lightweight tag with name \code{<name>},
			      \item \code{git tag -a <name> -m <message>} to create an annotated
			            tag with name \code{<name>} and message \code{<message>},
			      \item \code{git tag [-a] <name> <commit>}  to create a tag on commit \code{<commit>}.
		      \end{itemize}
	\end{itemize}
	\begin{consoleoutput}
		$ git log --oneline
		a06ace4 (HEAD -> main, tag: v2.0.0, tag: Nature_2024_JDoe_et_al) Finalize realease
		11e5bed Add unit tests
		87900b6 Improve documentation
		0a2253b (tag: v1.1.0) Add feature A
		e792bb1 (tag: v1.0.1) Fix bug
		ce7cf28 (tag: v1.0.0) Initial commit
	\end{consoleoutput}
\end{frame}

\subsubsection{Working with branches}
\begin{frame}[fragile,t]
	\frametitle{Working with branches}
	\framesubtitle{Managing branches}

	\begin{itemize}
		\item One of the Git strengths is its ability to support nonlinear
		      workflows.
		\item You can easily diverge from the main development line without
		      impacting it.
		\item Git does that using \textit{branches}.
		\item Before diving into branches, let's see how Git manages the references
		      to commits.
	\end{itemize}
	\addimage[width=7cm]{\FIGREP/branch-and-history}{4.5cm}{1cm}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Working with branches}
	\framesubtitle{Managing branches}

	\begin{itemize}
		\item To isolate your work from the main development, create a branch using
		      \code{git branch}:
		      \begin{consoleoutput}
			      $git branch testing
		      \end{consoleoutput}
	\end{itemize}
	\addimage[width=7cm]{\FIGREP/head-to-master}{4.5cm}{1cm}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Working with branches}
	\framesubtitle{Managing branches}

	\begin{itemize}
		\item Let's switch to the new branch using \code{git switch}:
		      \begin{consoleoutput}
			      $git switch testing
			      Switched to branch 'testing'
		      \end{consoleoutput}
	\end{itemize}
	\addimage[width=7cm]{\FIGREP/head-to-testing}{4.5cm}{1cm}
\end{frame}

\begin{frame}[fragile,t]
	\frametitle{Working with branches}
	\framesubtitle{Managing branches}

	\begin{itemize}
		\item We can now work on our branches separately without affecting the other.
	\end{itemize}
	\onslide<1>\addimage[width=7cm]{\FIGREP/advance-testing}{4.5cm}{2cm}
	\onslide<2>\addimage[width=7cm]{\FIGREP/advance-master}{4.5cm}{2cm}
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{Working with branches}
	\framesubtitle{Merging branches}

	\begin{itemize}
		\item Once you are satisfied with the work done on your branch, you can
		      include it back to the main development branch.
		\item This is called merging. Use \code{git merge <branch to merge>}
	\end{itemize}
	\onslide<1>\addimage[width=7cm]{\FIGREP/basic-branching-1}{4.5cm}{2cm}
	\onslide<2>\addimage[width=7cm]{\FIGREP/basic-branching-2}{4.5cm}{2cm}
	\onslide<3>\addimage[width=7cm]{\FIGREP/basic-branching-3}{4.5cm}{2cm}
	\onslide<4>\addimage[width=7cm]{\FIGREP/basic-branching-4}{4.5cm}{2cm}
	\onslide<5>\addimage[width=7cm]{\FIGREP/basic-branching-5}{4.5cm}{2cm}
	\onslide<6>\addimage[width=7cm]{\FIGREP/basic-branching-6}{4.5cm}{2cm}
	\onslide<7>\addimage[width=7cm]{\FIGREP/basic-merging-2}{4.5cm}{2cm}
\end{frame}

\begin{frame}[fragile, t, exercise]
	\frametitle{Working with branches}
	\framesubtitle{Merging branches}

	\begin{itemize}
		\item Reproduce the previous situation from scratch
		      \begin{itemize}
			      \item Create 3 commits.
			      \item Add a new \code{iss53} branch, switch to it, and make a commit..
			      \item Switch back to \code{main}, create an \code{hotfix} branch and
			            switch to it.
			      \item Merge \code{hotfix} into \code{main}.
			      \item Finally, merge \code{iss53} into \code{main}.
		      \end{itemize}
	\end{itemize}
	\vfill
	\begin{itemize}
		\item On the \code{main} branch, create a file \code{data.txt} and write
		      ``This is the first line'' in it.
		\item Commit this file.
		\item Create a new branch called \code{feature}.
		\item Still on \code{main}, add ``This is a new line from main'' into
		      \code{data.txt} and commit it.
		\item Now, go to \code{feature} and add ``This is a new line from feature'' into
		      \code{data.txt} and commit it.
		\item Merge \code{feature} into \code{main}.
		\item What happens? Use \code{git status} and inspect \code{data.txt} with
		      an editor.
	\end{itemize}
\end{frame}

\begin{frame}[fragile, t]
	\frametitle{Working with branches}
	\framesubtitle{Merging branches}

	\begin{itemize}
		\item You just witnessed a merging conflict!
		\item When there are changes in the same lines, Git doesn't know which one
		      it should accept.
		\item You have to manually solve the conflicts!
		      \begin{itemize}
			      \item Modify the regions marked with \code{<{}<{}<{}<{}<{}<{}<}, \code{=======},
			            and \code{>{}>{}>{}>{}>{}>{}>}.
			      \item Once everything is solved, type \code{git commit}.
		      \end{itemize}
		\item Options exist to tell Git what to do.
	\end{itemize}
\end{frame}

\subsection{Interacting with the world!}
\begin{frame}[fragile]
	\frametitle{Interacting with the world!}
	\framesubtitle{Remotes}

	\begin{itemize}
		\item Up until now, all the operations we have looked at are \texttt{local}
		      to our repository.
		\item By its decentralized nature, Git allows you to share work with
		      different special repos called \texttt{remotes}.
	\end{itemize}
\end{frame}

\subsubsection{Showing the remotes}
\begin{frame}[fragile]
	\frametitle{Showing the remotes}
	\framesubtitle{\code{git remote}}

	\begin{itemize}
		\item The command \code{git remote [show <name>]} will print the information about the
		      remotes.
		      \begin{consoleoutput}
			      $ git remote
			      origin
		      \end{consoleoutput}
		      \begin{consoleoutput}
			      $ git remote show origin
			      * remote origin
			      Fetch URL: git@gitlab.epfl.ch:SCITAS/courses/data-management-with-git.git
			      Push  URL: git@gitlab.epfl.ch:SCITAS/courses/data-management-with-git.git
			      HEAD branch: main
			      Remote branches:
			      main          tracked
			      Local branch configured for 'git pull':
			      main merges with remote main
			      Local ref configured for 'git push':
			      main pushes to main (up to date)
		      \end{consoleoutput}
		\item By default, the first remote is always called \code{origin}.
	\end{itemize}
\end{frame}

\subsubsection{Adding a remote}
\begin{frame}[fragile]
	\frametitle{Adding a remote}
	\framesubtitle{\code{git remote add}}

	\begin{itemize}
		\item You can add and remove remotes at your will with \code{git remote add/remove}.
		\item For example, you started a local remote and you want to push it to
		      GitLab or Github.
		      \begin{consoleoutput}
			      $ git remote add <name> <repo address>
		      \end{consoleoutput}
		\item Similarly, you may remove a repo:
		      \begin{consoleoutput}
			      $ git remote remove <name>
		      \end{consoleoutput}
	\end{itemize}
\end{frame}

\subsubsection{Getting changes from the remotes}
\begin{frame}[fragile]
	\frametitle{Getting changes from the remotes}
	\framesubtitle{\code{git fetch}}

	\begin{itemize}
		\item To get updates from your remote, you can use \code{git fetch}.
		\item The command will download all the changes from a remote.
		\item It is important to note that \code{git fetch} \texttt{will not merge} the
		      changes into your local copy.
		      \begin{consoleoutput}
			      $ git fetch origin
			      $ git log --graph --oneline --all
			      * 83a2e8e (origin/main) Add content
			      * a06ace4 (HEAD -> main, tag: v2.0.0, tag: Nature_2024_JDoe_et_al) Finalize release
			      * 11e5bed Add unit tests
			      * 87900b6 Improve documentation
			      * 0a2253b (tag: v1.1.0) Add feature A
			      * e792bb1 (tag: v1.0.1) Fix bug
			      * ce7cf28 (tag: v1.0.0) Initial commit
		      \end{consoleoutput}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Getting changes from the remotes}
	\framesubtitle{\code{git pull}}

	\begin{itemize}
		\item To actually include the updates into your local copy, you can use
		      \code{git pull <remote> <branch>}
		      \begin{consoleoutput}
			      $ git pull origin main
			      From <remote address>
			      * branch            main       -> FETCH_HEAD
			      Updating a06ace4..83a2e8e
			      Fast-forward
			      file_5.txt | 1 +
			      1 file changed, 1 insertion(+)
		      \end{consoleoutput}
		      \begin{consoleoutput}
			      $ git log --graph --oneline --all
			      * 83a2e8e (HEAD -> main, origin/main) Add content
			      * a06ace4 (tag: v2.0.0, tag: Nature_2024_JDoe_et_al) Finalize realease
			      * 11e5bed Add unit tests
		      \end{consoleoutput}
		\item In fact \code{git pull} is equivalent to \code{git fetch} followed by
		      a \code{git merge}.
	\end{itemize}
\end{frame}

\subsubsection{Pushing your work to the remotes}
\begin{frame}[fragile]
	\frametitle{Pushing your work to the remotes}
	\framesubtitle{\code{git push}}

	\begin{itemize}
		\item At some point, you may want to share your work with your colleagues.
		\item To do so, you must push your changes to the remote using \code{git
			      push <remote> <branch>}.
		      \begin{consoleoutput}
			      $ git push origin main
			      Enumerating objects: 5, done.
			      Counting objects: 100% (5/5), done.
			      Delta compression using up to 8 threads
			      Compressing objects: 100% (2/2), done.
			      Writing objects: 100% (3/3), 260 bytes | 260.00 KiB/s, done.
			      Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
			      To <remote address>
			      a06ace4..83a2e8e  main -> main
		      \end{consoleoutput}
	\end{itemize}
\end{frame}

% \subsubsection{Hosting and developer platforms}
% \begin{frame}[fragile]
% 	\frametitle{}
% 	\framesubtitle{}

% \end{frame}

\subsection{Configuring Git}
\begin{frame}[fragile]
	\frametitle{Configuring Git}
	\framesubtitle{\code{git config}}

	\begin{itemize}
		\item You can easily configure Git to make it easier to work with.
		\item There are three levels of configuration:
		      \begin{description}
			      \item[\code{-{}-system}] This will affect Git for all the users of
				      the machine. The configuration is stored in \code{/etc/gitconfig}
			      \item[\code{-{}-global}] This will only affect you and the
				      configuration file is stored in \code{<home>/.git/config} or \code{<home>/.gitconfig}.
			      \item[\code{-{}-local}] This will only affect the current
				      repository. The configuration file is stored in \code{<project>/.git/config}.
		      \end{description}
		\item To set an option, use \code{git config -{}-<level> <option> <value>}.
		      For example:
		      \begin{consoleoutput}
			      $ git config --global user.name "John Doe"
			      $ git config --global user.email john.doe@epfl.ch
			      $ git config --global core.editor emacs
			      $ git config --global alias.graph log --oneline --graph --all
		      \end{consoleoutput}
		\item Use \code{git config -{}-list} to see your configuration.
	\end{itemize}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../data_management"
%%% End:
