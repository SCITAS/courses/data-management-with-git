\renewcommand{\FIGREP}{src/data_transfers/figures}

\section{Data management at SCITAS}
\label{sec:data_management}
\intersec{helvetios}

\subsection{SCITAS' file systems}
\begin{frame}[fragile]
	\frametitle{SCITAS' file systems}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item There are seven different file systems on the clusters.
		\item Each of them has a purpose for which it is best suited for.
		\item But also particularities that could lead to data loss if not used
		      correctly.
		\item It is then very important to know the characteristics of each in order to
		      efficiently use the clusters.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{There is nothing like \code{/home}}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/home} is the default file system on which you are when you log
		      into the machines.
		\item It is used to store relatively small files that you want to conserve, \eg{} your
		      source codes, configuration files, and input files.
		      \pause
		\item There is a quota of \SI{100}{\giga\byte} (use the \code{fsu} command
		      to check it).
					\begin{itemize}
						\item Note that due to data replication, \code{fsu} will print twice
									your usage.
					\end{itemize}
		\item It is accessible from all the clusters.
		      \pause
		\item Data is backed up
		\item Data will be erased after two years of inactivity or six months after
		      leaving EPFL.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Share your \code{/work} with your colleagues}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/work} is a team storage that is accessible by all your
		      colleagues.
		\item It is ideal to store large data such as your simulation results
		      for several years.
		      \pause
		\item There is no quota. Storage is based on a pay-per-use model.
		\item It is accessible from all the clusters.
		      \pause
		\item Data is not backed up by default, but you can activate it by creating
		      a file called \code{.backup} at the root of your \code{/work},
		      \eg{}\newline
		      \code{> touch /work/scitas/.backup}
		\item Data will be erased six months after cessation of payment.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Let's \code{/archive} your data}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/archive} is a particular file system that is stored on tape.
		\item It is used to store data for very-long time.
		      \pause
		\item There is no quota. Storage is based on a pay-per-use model.
		\item It is accessible from all the clusters.
		      \pause
		\item There is no backup, but data is replicated on two different
		      sites.
		\item Data will be erased six months after cessation of payment.
	\end{itemize}
	\pause
	\vfill
	\begin{itemize}
		\item Used for cold storage and not frequent IO. There is a limited
		      bandwidth (a robotic arm has to physically move tapes around).
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Let's \code{/transfer} your data outside of EPFL}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/transfer} is used to transfer your data outside of the EPFL network.
		      \pause
		\item There is no quota.
		\item It is accessible from all the clusters and the \code{fdata1.epfl.ch} node.
		      \pause
		\item There is no backup.
		\item Data will be erased after 15 days.
	\end{itemize}
	\pause
	\vfill
	\begin{itemize}
		\item We will see how it works in the next section.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\code{/export} your data}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/export} provides a storage space where your data can be exported
		      to other machines using the NFS, SMB, or S3 protocols.
		      \pause
		\item There is no quota. Storage is based on a pay-per-use model.
		\item It is accessible from all the clusters.
		      \pause
		\item Data is not backed up by default, but you can activate it by creating
		      a file called \code{.backup} at the root of your \code{/export},
		      \eg{}\newline
		      \code{> touch /export/scitas/.backup}
		\item Data will be erased six months after cessation of payment.
	\end{itemize}
\end{frame}



\begin{frame}[fragile]
	\frametitle{Write your simulation results into \code{/scratch}}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/scratch} is a temporary high-performance storage for your
		      simulation data.
		      \pause
		\item There is no quota.
		\item Each cluster has its own \code{/scratch}.
		      \pause
		\item There is no backup.
		\item Files older than 30 days will be erased without notice.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\code{/tmp}, but not forgettable}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\begin{itemize}
		\item \code{/tmp/\$SLURM\_JOB\_ID} is a node-local and temporary high-performance storage for your
		      simulation data.
		      \pause
		\item There is no quota.
		\item Each node has its own \code{/tmp}.
		      \pause
		\item There is no backup.
		\item Files are automatically removed after the run terminates.
	\end{itemize}
\end{frame}

\begin{frame}[b,fragile]
	\frametitle{SCITAS' file systems  summary}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/storage/file-system/}}

	\addimage[width=15cm]{\FIGREP/filesystems}{0.5cm}{0.5cm}
\end{frame}

\subsection{Storing your data}
\begin{frame}[fragile]
	\frametitle{Storing your data}
	\framesubtitle{\url{https://scitas-doc.epfl.ch/user-guide/data-management/how-to-use-filesystems/}}

	\addimage[width=13cm]{\FIGREP/store_data_flow}{2.0cm}{1.5cm}
\end{frame}

\section{Transferring data}
\label{sec:data_transfers}
\intersec{izar}

\subsection{Transferring data}
\begin{frame}[fragile]
	\frametitle{Transferring data}
	\framesubtitle{}
	There are different protocols that you can use to transfer data between two
	machines:
	\vspace{0.5cm}
	\begin{description}
		\item[\code{scp}] Copy files between a local and a remote host.
		\item[\code{rsync}] Synchronize files between two locations.
		\item[\code{sftp}] Protocol allowing the user file access, copy and
			management over the network.
		\item[\code{sshfs}] Mount a remote file system to use it as if it were local.
	\end{description}
	\vspace{0.5cm}
	They are all based of the SSH protocol.
\end{frame}

\begin{frame}[fragile]
	\frametitle{Transferring data using \code{rsync}}
	\framesubtitle{}

	\begin{itemize}
		\item \code{rsync} is a tool used to synchronize files.
		\item It compares the modification times and sizes of files to transfer only
		      the differences.
		\item If the transfer is interrupted, can be resumed from where it stopped.
		      \pause
		\item Usage:
		      \begin{itemize}
			      \item \code{rsync -auvP <source> <destination>}
			      \item \code{rsync -auvP <user>@<remote>:<source> <destination>}
			      \item \code{rsync -auvP <source> <user>@<remote>:<destination>}
		      \end{itemize}
		      where the options are
		      \begin{itemize}
			      \item \code{-a} for a recursive copy preserving
			            modification times, links, permissions, etc.
			      \item \code{-u} to preserve
			            files that are newer on the destination,
			      \item \code{-v} to have a verbose
			            output,
			      \item \code{-P} to keep partial files and print the progress.
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile, exercise]
	\frametitle{Transferring data using \code{rsync}}
	\framesubtitle{Using \code{rsync}}

	\begin{itemize}
		\item Create a temporary folder, \code{tmp/}, in your \code{/home}.
		\item Copy the folder \code{/work/scitas-share/cmake-3} in your \code{tmp/}
		      folder:\\
		      \code{cp -r /work/scitas-share/cmake-3 tmp/}
		\item Create a backup folder, \code{backup/}, in your \code{tmp/} folder.
		\item Use \code{rsync} to create a backup of \code{tmp/cmake-3} into
		      \code{tmp/backup}.
		\item Add a random modification in
		      \code{tmp/cmake-3/doc/cmake-3.11/Copyright.txt}
		\item Re-synchronize \code{tmp/cmake-3} into \code{tmp/backup}.
		\item Transfer the backup folder to your local machine.
		\item What happens if you add a trailing slash to the source directory,
		      \ie{} the difference between the following two\newline
		      \code{rsync -auvP source destination}
		      \newline and \newline
		      \code{rsync -auvP source/ destination}
	\end{itemize}
\end{frame}

\subsection{Transferring data to/from the outside of EPFL network}
\begin{frame}[fragile]
	\frametitle{Transferring data to/from the outside of EPFL network}	\framesubtitle{https://scitas-doc.epfl.ch/user-guide/data-management/how-to-use-filesystems/\#transferring-data}

	\begin{itemize}
		\item You may sometimes need to transfer data between the SCITAS' clusters
		      and other machines outside of EPFL network, \eg{} another HPC center.
		\item To do so, we have a transfer node, \code{fdata1.epfl.ch}, with limited capabilities accessible
		      from outside of EPFL network.
		\item For security reasons, only the \code{/transfer} file system is mounted
		      on \code{fdata1}.
		\item To transfer your data from EPFL to another center, you typically need
		      to do the following:
		      \begin{itemize}
			      \item From a cluster, copy the data you wish to transfer into
			            \code{/transfer}.
			      \item Log into the destination machine and transfer the files using your
			            preferred protocol, \eg{}\newline
			            \code{rsync -auvP <user>@fdata1.epfl.ch:/transfer/<folder>
				            <local folder>}
		      \end{itemize}
	\end{itemize}
\end{frame}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../data_management"
%%% End:
