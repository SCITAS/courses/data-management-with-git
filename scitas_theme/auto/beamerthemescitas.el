(TeX-add-style-hook
 "beamerthemescitas"
 (lambda ()
   (TeX-run-style-hooks
    "tikz"
    "etoolbox")
   (TeX-add-symbols
    '("intersec" 1)
    '("addimage" 5)))
 :latex)

