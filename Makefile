SHELL := /bin/bash

slides:
	pdflatex -shell-escape data_management.tex
	pdflatex -shell-escape data_management.tex
	pdflatex -shell-escape data_management.tex

clean:
	rm -rf *.{aux,log,nav,out,snm,toc,vrb,listing} _minted-data_management data_management.synctex.gz

clean-all: clean
	rm -f data_management.pdf
